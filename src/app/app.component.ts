import { Component, OnInit } from '@angular/core';
import { CountriesService } from './services/countries.service';
import { CitiesService } from './services/cities.service';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  places = [];

  constructor(
    private _countriesService: CountriesService,
    private _citiesService: CitiesService
  ) {}

  ngOnInit() {

    const callbackHell = this._citiesService.getCities().pipe(switchMap((a: any) => 
        this._countriesService.getCountries().pipe(map(b => {
          return [a, b]
        }))
      ) 
    );

    callbackHell.subscribe(res => {
      let cities = res[0];
      let countries = res[1];

      console.log('cities.length', cities.length)
      for (var i = 0, max = cities.length; i < max; ++i) {
        this.places.push({country: countries[i], city: cities[i]})
      }
    })
    
  }
}
