import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  cities: Subject<any> = new BehaviorSubject(['Warsaw', 'Rome', 'Praga']);

  constructor() { }

  getCities() {
    return this.cities.asObservable();
  }

}
