import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  
  countries: Subject<any> = new BehaviorSubject(['Poland', 'Italia', 'Czech']);

  constructor() { }

  getCountries(): Observable<any> {
    return this.countries.asObservable();
  }


}
